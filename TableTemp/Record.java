public class Record {
  private int id;
  private float temperature;
  public Record(int id,float temperature) {
     this.id=id;
     this.temperature=temperature;
  }
  public int getId() {
    return id;
  }
  public float getTemperature() {
    return temperature;
  }
}
