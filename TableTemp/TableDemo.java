import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.Scene;
import javafx.application.Application;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.cell.PropertyValueFactory;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.*;
import java.util.Collections;

class Controller {
    private ObservableList<Record> data = FXCollections.observableArrayList();
    private TableView<Record> table;
    private TableColumn<Record, Integer> idCol;
    private TableColumn<Record, Float> tempCol;

    public Controller(TableView<Record> table,
                      TableColumn<Record, Integer> idCol,
                      TableColumn<Record, Float> tempCol) {
      this.table=table;
      this.idCol=idCol;
      this.tempCol=tempCol;
      initialize();

    }
    public ObservableList<Record> getData() {
      return data;
    }
    private void initialize() {
        initData();
        // устанавливаем тип и значение которое должно хранится в колонке
        idCol.setCellValueFactory(new PropertyValueFactory<Record, Integer>("id"));
        tempCol.setCellValueFactory(new PropertyValueFactory<Record, Float>("temperature"));
        // заполняем таблицу данными
        table.setItems(data);
    } 
        // подготавливаем данные для таблицы
    // вы можете получать их с базы данных
    private void initData() {
       MysqlConnect mysqlConnect = new MysqlConnect();
       String sql = "SELECT * FROM `data`";
       try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
               String idString = rs.getString("id");
               String tString = rs.getString("temperature");
               //System.out.println(idString+" "+tString);
               data.add(new Record(Integer.parseInt(idString),
                                   Float.parseFloat(tString)));
             }
             Collections.reverse(data);
       
       } catch (SQLException e) {
          e.printStackTrace();
       } finally {
          mysqlConnect.disconnect();
       }       
        
    }
}


public class TableDemo extends Application{
    @Override
    public void start(Stage primaryStage) {
      TableView<Record> table = new TableView<Record>();
      TableColumn<Record,Integer> col1 = new TableColumn<>("ID");
      TableColumn<Record,Float> col2 = new TableColumn<>("Температура");
      col1.setPrefWidth(45);
      col2.setPrefWidth(100);
      table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
      table.getColumns().addAll(col1, col2);

      Controller controller=new Controller(table,col1,col2);
      

      final NumberAxis xAxis = new NumberAxis();
      final NumberAxis yAxis = new NumberAxis();
      xAxis.setLabel("Номер измерения");
      yAxis.setLabel("T,C");
      LineChart<Number,Number> lineChart = 
                new LineChart<Number,Number>(xAxis,yAxis);

      XYChart.Series series = new XYChart.Series();

      ObservableList<Record> data=controller.getData();
      for(Record rec: data) {
         series.getData().add(new XYChart.Data(rec.getId(), rec.getTemperature()));
       }
      lineChart.getData().add(series);
      lineChart.setCreateSymbols(false);
      

      BorderPane root = new BorderPane();
      HBox hbox = new HBox();
      hbox.getChildren().add(table);
      hbox.getChildren().add(lineChart);
      HBox.setHgrow(lineChart, Priority.ALWAYS);

      root.setCenter(hbox);

      primaryStage.setScene(new Scene(root));
      primaryStage.setWidth(1200);
      primaryStage.setHeight(600);
      primaryStage.setResizable(false);
      primaryStage.show();


    }
    public static void main(String[] args) {
      launch(args);
    }
}