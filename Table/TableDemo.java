import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.Scene;
import javafx.application.Application;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.cell.PropertyValueFactory;


class Controller {
    private ObservableList<Task> data = FXCollections.observableArrayList();
    private TableView<Task> table;
    private TableColumn<Task, Integer> idCol;
    private TableColumn<Task, String> titleCol;

    public Controller(TableView<Task> table,
                      TableColumn<Task, Integer> idCol,
                      TableColumn<Task, String> titleCol) {
      this.table=table;
      this.idCol=idCol;
      this.titleCol=titleCol;
      initialize();

    }
    private void initialize() {
        initData();
        // устанавливаем тип и значение которое должно хранится в колонке
        idCol.setCellValueFactory(new PropertyValueFactory<Task, Integer>("id"));
        titleCol.setCellValueFactory(new PropertyValueFactory<Task, String>("title"));
        // заполняем таблицу данными
        table.setItems(data);
    } 
        // подготавливаем данные для таблицы
    // вы можете получать их с базы данных
    private void initData() {
        data.add(new Task(1, "Сходить на работу"));
        data.add(new Task(2, "Купить продукты"));
        data.add(new Task(3, "Вымыть пол"));
        data.add(new Task(4, "Заплатить за квартиру"));
        data.add(new Task(5, "Проверить BitBucket"));
    }
}


public class TableDemo extends Application{
    @Override
    public void start(Stage primaryStage) {
      TableView<Task> table = new TableView<Task>();
      TableColumn<Task,Integer> col1 = new TableColumn<>("ID");
      TableColumn<Task,String> col2 = new TableColumn<>("Задача");
      col1.setPrefWidth(75);
      col2.setPrefWidth(175);
      table.getColumns().addAll(col1, col2);


      Controller controller=new Controller(table,col1,col2);

      BorderPane root = new BorderPane();
      root.setCenter(table);
      primaryStage.setScene(new Scene(root));
      primaryStage.setWidth(400);
      primaryStage.setHeight(400);
      primaryStage.setResizable(false);
      primaryStage.show();


    }
    public static void main(String[] args) {
      launch(args);
    }
}