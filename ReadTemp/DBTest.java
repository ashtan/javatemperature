import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

// Запуск: java -cp mysql-connector-java-5.1.39-bin.jar:. DBTest
class MysqlConnect {
    // init database constants
    private static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DATABASE_URL = "jdbc:mysql://192.168.0.128:3306/weatherNN?useSSL=false";
    private static final String USERNAME = "anton";
    private static final String PASSWORD = "1234";
    private static final String MAX_POOL = "250";

    // init connection object
    private Connection connection;
    // init properties object
    private Properties properties;

    // create properties
    private Properties getProperties() {
        if (properties == null) {
            properties = new Properties();
            properties.setProperty("user", USERNAME);
            properties.setProperty("password", PASSWORD);
            properties.setProperty("MaxPooledStatements", MAX_POOL);
        }
        return properties;
    }

    // connect database
    public Connection connect() {
        if (connection == null) {
            try {
                Class.forName(DATABASE_DRIVER);
                connection = DriverManager.getConnection(DATABASE_URL, 
                                                         getProperties());
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    // disconnect database
    public void disconnect() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

public class DBTest 
{
	public static void main(String[] args) {
       MysqlConnect mysqlConnect = new MysqlConnect();
       String sql = "SELECT * FROM `data`";
       try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
			while (rs.next()) {

				String idString = rs.getString("id");
				String tString = rs.getString("temperature");

				System.out.print("id : " + idString);
				System.out.println(" T : " + tString);

			}
       } catch (SQLException e) {
          e.printStackTrace();
       } finally {
          mysqlConnect.disconnect();
       } 
	}
}
